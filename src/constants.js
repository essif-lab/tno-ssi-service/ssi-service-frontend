export const CONNECTORS = [
  {
    name: "jolocom",
    title: "Jolocom",
    imageUrl: require("./assets/connector-jolocom-logo.svg"),
    appleDownloadUrl:
      "https://apps.apple.com/us/app/jolocom-smartwallet/id1223869062",
    googleDownloadUrl:
      "https://play.google.com/store/apps/details?id=com.jolocomwallet",
  },
  {
    name: "irma",
    title: "IRMA",
    imageUrl: require("./assets/connector-irma-logo.svg"),
    appleDownloadUrl:
      "https://apps.apple.com/nl/app/irma-authenticatie/id1294092994",
    googleDownloadUrl:
      "https://play.google.com/store/apps/details?id=org.irmacard.cardemu",
  },
  {
    name: "sovrin",
    title: "Connect.Me",
    imageUrl: "https://placekitten.com/300/200",
    appleDownloadUrl: "https://apps.apple.com/us/app/connect-me/id1260651672",
    googleDownloadUrl:
      "https://play.google.com/store/apps/details?id=me.connect",
  },
];
